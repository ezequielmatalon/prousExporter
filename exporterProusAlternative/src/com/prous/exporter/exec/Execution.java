package com.prous.exporter.exec;

import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Struct;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.math.RandomUtils;
import org.springframework.web.util.HtmlUtils;
import org.w3c.dom.Document;

import com.bea.xml.stream.XMLOutputFactoryBase;
import com.prous.exporter.bean.ExperimentalActivity;
import com.prous.exporter.bean.ExperimentalStudy;
import com.prous.exporter.bean.Source;
import com.prous.exporter.bean.TargetConditionToxicity;
import com.prous.logger.Logger;

public class Execution {

	private static final String TestingPath = "C:\\exportFiles\\file.xml";
	private static XMLStreamWriter xmlw;
	private static DocumentBuilderFactory docFactory = DocumentBuilderFactory
			.newInstance();
	private static DocumentBuilder docBuilder;
	private static Document doc;
	private static final String firstQuery = "SELECT emr_id, entry_number FROM (SELECT prp_relat_product AS entry_number FROM prous_web_content.pro_products, prous_web_content.pro_relat_products, prous_web_content.pro_relat_types WHERE prp_relat_product = pro_entry_number AND prt_relat_type_id = prp_relat_type_id AND prt_class_id = 3 AND pro_status_id = 3) tt, prous_web_content.exp_meth_results, prous_web_content.exp_meth_studies WHERE emr_entry_number = tt.entry_number AND emr_experiment_id = ems_experiment_id AND ems_status_id = 3 UNION SELECT emr_id, emr_entry_number FROM prous_web_content.exp_meth_results, prous_web_content.exp_meth_studies WHERE emr_experiment_id = ems_experiment_id AND ems_status_id = 3 AND EXISTS (SELECT 1 FROM prous_web_content.pro_products WHERE pro_status_id = 3 AND pro_entry_number = emr_entry_number)";
	private static String SQL = "SELECT emr_id, ems_ref_id, ems_pat_family_id, emr_entry_number, (SELECT ppn_name FROM prous_web_content.pro_product_names WHERE ppn_entry_number = emr_entry_number AND ppn_main_mfline = 'Y' AND rownum < 2) AS mainName, emr_experiment_id, emr_operator, emr_value1, emr_range, to_number(decode(emr_range, NULL, NULL, emr_value2)) AS emr_value2, exa_description AS activity, ltrim(ema_description) AS Material, ltrim(eme_description) AS Methode, epa_description AS Parameter, ltrim(uni_description) AS units, EXTRACCIONS.experimental_pkg.getValueAsHtml(emr_value1, emr_value2, NULL, emr_operator, emr_range, uni_description) AS value_as_html, EXTRACCIONS.experimental_pkg.getValueUsAscii(emr_value1, emr_value2, NULL, emr_operator, emr_range, uni_description) AS value_as_UsAscii, decode(ems_ref_id, NULL, 'Patent', 'Literature') AS SourceType, decode(ems_ref_id, NULL, (EXTRACCIONS.citations_pkg.getPatFullCitation(ems_pat_family_id)), (EXTRACCIONS.citations_pkg.getRefFullCitation(ems_ref_id))) AS SOURCE, decode(ems_ref_id, NULL, ems_pat_family_id, ems_ref_id) AS referenceOrPatFamilyId :yesParameter: FROM prous_web_content.exp_meth_results emr, prous_web_content.exp_meth_studies, prous_web_content.exp_meth_activities, prous_web_content.exp_materials, prous_web_content.exp_methodes, prous_web_content.exp_parameters, prous_web_content.valid_param_units, prous_web_content.units_mfline WHERE emr_experiment_id = ems_experiment_id AND ems_status_id = 3 AND ems_valid_unit_id = vpu_id AND vpu_unit_id = uni_unit_id AND ems_activity_id = exa_activity_id(+) AND ems_material_id = ema_material_id(+) AND ems_methode_id = eme_methode_id(+) AND ems_param_id = epa_parameter_id(+) AND EXISTS (SELECT 1 FROM (  :queryParameter:  ) t WHERE t.emr_id = emr.emr_id) AND ROWNUM<=5000";
	private static Connection con;
	private static Logger log;

	public static void main(String[] args) throws SQLException, IOException {

		con = getDBConnection();

		try {
			docBuilder = docFactory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}
		doc = docBuilder.newDocument();

		try {
			execute(args[0], args[1], args[2] == "Y");
			// uncomment this line for testing
			// execute(firstQuery, TestingPath, Boolean.FALSE);
		} catch (XMLStreamException | SQLException | IOException e) {
			con.close();
			e.printStackTrace();
		}
		con.close();

	}

	public static void execute(String sql, String filepath, Boolean extraInfo)
			throws XMLStreamException, SQLException, IOException {

		/**
		 * 1)Create first XML tag 2)Require DB connection and run million and
		 * half register's query adding extra fields in positive case (parameter
		 * on 'Y') 3)Looping through the cursor to create an object and add it
		 * into the XML 4)Finish each XML tag, close DB connection, finish the
		 * procces
		 * 
		 * 
		 */
	
		// Code for testing
	
		// if (extraInfo) {
		// filepath = filepath.replaceAll(":name:",
		// String.valueOf(RandomUtils.nextInt()) + "withExpActivity");
		// } else {
		// filepath = filepath.replaceAll(":name:",
		// String.valueOf(RandomUtils.nextInt()));
		// }

		log = new Logger(filepath.concat(".txt"));
		String finalQuery = SQL.replaceAll(":queryParameter:", sql);
		if (extraInfo) {
			finalQuery = finalQuery
					.replaceAll(":yesParameter:",
							", extraccions.GET_EXP_ACTIVITY_DATA(emr_id) as exp_activity ");
		} else {
			finalQuery = finalQuery.replaceAll(":yesParameter:", "");
		}
		System.out.println(finalQuery);
		log.write(finalQuery);

		//
		// Get an output factory
		//
		XMLOutputFactory xmlof = XMLOutputFactory.newInstance();
		xmlof.setProperty("escapeCharacters", false);
		System.out.println("FACTORY: " + xmlof);

		//
		// Instantiate a writer
		//
		xmlw = xmlof.createXMLStreamWriter(new FileOutputStream(filepath));
		
		System.out.println("READER:  " + xmlw + "\n");

		//
		// Generate the XML
		//
		// Write the default XML declaration
		Date startingDate = new Date();
		xmlw.writeStartDocument();
		xmlw.writeCharacters("\n");
		xmlw.writeCharacters("\n");

		// Write the root element "Related_Experiments"
		xmlw.writeStartElement("Related_Experiments");
		xmlw.writeCharacters("\n");

		Statement stmt = con.createStatement();
		stmt.setFetchSize(5000);
		ResultSet rs = stmt.executeQuery(finalQuery);
		int status = 0;
		int regAmm = 0;
		System.out.println("Starting export at " + startingDate.toString());
		log.write("Starting export at " + startingDate.toString());

		while (rs.next()) {
			ExperimentalStudy expStudy = new ExperimentalStudy();
			status++;
			regAmm++;
			if (extraInfo) {
				ResultSet targetActivity = rs.getArray("exp_activity")
						.getResultSet();
				targetActivity.setFetchSize(100);
				while (targetActivity.next()) {
					ExperimentalActivity expa = new ExperimentalActivity();

					Struct structObject = (Struct) targetActivity.getObject(2);
					Object[] attributes = structObject.getAttributes();
					List<Object> lis =new ArrayList<Object>(Arrays.asList(attributes));
					
					Object system=null, effect=null, tarType=null, tarCont=null;
					try {
					    system = lis.get(2);
					} catch ( IndexOutOfBoundsException e ) {
					    
					}
					
					try {
					    effect = lis.get(3);
					} catch ( IndexOutOfBoundsException e ) {
					    
					}
					
					try {
						tarType = lis.get(4);
					} catch ( IndexOutOfBoundsException e ) {
					    
					}
					
					try {
						tarCont = lis.get(5);
					} catch ( IndexOutOfBoundsException e ) {
					    
					}
					
					if(system!=null){
					expa.setSystem(system.toString());
					}
					
					if(effect!=null){
					expa.setEffect(effect.toString());
					}
					
					TargetConditionToxicity tarCond = new TargetConditionToxicity();
					if(tarType!=null){
						tarCond.setType(tarType.toString());
					}
					
					if(tarCont!=null){
					tarCond.setContent(tarCont.toString());
					}
					
					
					expa.setTargetConditionToxicity(tarCond);
					expStudy.getExperimentalActivity().add(expa);

				}
			}
			expStudy.setEntryNumber(new BigInteger(rs
					.getString("emr_entry_number")));
			expStudy.setId(new BigInteger(rs.getString("emr_id")));
			expStudy.setMaterial(rs.getString("Material"));
			expStudy.setMethod(HtmlUtils.htmlEscapeDecimal(rs
					.getString("Methode")));
			expStudy.setOperator(HtmlUtils.htmlEscapeDecimal(rs
					.getString("emr_operator")));
			expStudy.setParameter(rs.getString("Parameter"));
			expStudy.setPharmacologicalActivity(rs.getString("activity"));
			expStudy.setRangeOperator(HtmlUtils.htmlEscapeDecimal(rs
					.getString("emr_range")));
			Source src = new Source();
			src.setType(rs.getString("sourceType"));
			src.setValue(new BigInteger(rs.getString("referenceOrPatFamilyId")));
			expStudy.setSource(src);
			expStudy.setUnits(HtmlUtils.htmlEscapeDecimal(rs
					.getString("units")));
			expStudy.setValue(HtmlUtils.htmlEscapeDecimal((rs
					.getString("value_as_html"))));
			expStudy.setValue1(rs.getDouble("emr_value1"));
			expStudy.setValue2(rs.getDouble("emr_value2"));

			System.out.println("Adding emr_id=" + expStudy.getId()
					+ " register number:" + regAmm);
			log.write("Adding emr_id=" + expStudy.getId() + " register number:"
					+ regAmm);
			AddStudy(expStudy);
			xmlw.flush();
			if (status == 10000) {

				// in milliseconds
				long diff = new Date().getTime() - startingDate.getTime();

				long diffMinutes = diff / (60 * 1000);
				System.out.println("It took " + diffMinutes
						+ " minutes to this point");
				log.write("It took " + diffMinutes + " minutes to this point");
				System.gc();
				status = 0;
			}
		}

		// End the "Related_Experiments" element
		xmlw.writeEndElement();
		// End the XML document
		xmlw.writeEndDocument();
		// Close the XMLStreamWriter to free up resources
		xmlw.close();

		System.out.println("Finishing export at " + new Date().toString());
		log.write("Finishing export at " + new Date().toString());
		log.close();

	}

	private static void AddStudy(ExperimentalStudy study)
			throws XMLStreamException {

		xmlw.writeCharacters("    ");
		xmlw.writeStartElement("Experimental_Study");
		xmlw.writeAttribute("Id", study.getId().toString());
		xmlw.writeCharacters("\n");

		if(study.getEntryNumber() != null){
		xmlw.writeCharacters("        ");
		xmlw.writeStartElement("Entry_Number");
		xmlw.writeCharacters(study.getEntryNumber().toString());
		xmlw.writeEndElement();
		xmlw.writeCharacters("\n");
		}
		

		if(study.getPharmacologicalActivity() != null && !study.getPharmacologicalActivity().isEmpty()){
		xmlw.writeCharacters("        ");
		xmlw.writeStartElement("Pharmacological_Activity");
		xmlw.writeCharacters(study.getPharmacologicalActivity());
		xmlw.writeEndElement();
		xmlw.writeCharacters("\n");
		}

		if(study.getMaterial() != null && !study.getMaterial().isEmpty()){
		xmlw.writeCharacters("        ");
		xmlw.writeStartElement("Material");
		xmlw.writeCharacters(study.getMaterial());
		xmlw.writeEndElement();
		xmlw.writeCharacters("\n");
		}
		
		if(study.getMethod() != null && !study.getMethod().isEmpty()){
		xmlw.writeCharacters("        ");
		xmlw.writeStartElement("Method");
		xmlw.writeCharacters(study.getMethod());
		xmlw.writeEndElement();
		xmlw.writeCharacters("\n");
		}

		if(study.getParameter() != null && !study.getParameter().isEmpty()){
		xmlw.writeCharacters("        ");
		xmlw.writeStartElement("Parameter");
		xmlw.writeCharacters(study.getParameter());
		xmlw.writeEndElement();
		xmlw.writeCharacters("\n");
		}

		if(study.getValue1() !=0.0d){
		xmlw.writeCharacters("        ");
		xmlw.writeStartElement("Value1");
		xmlw.writeCharacters(String.valueOf(study.getValue1()));
		xmlw.writeEndElement();
		xmlw.writeCharacters("\n");
		}

		if(study.getRangeOperator() != null && !study.getRangeOperator().isEmpty()){
		xmlw.writeCharacters("        ");
		xmlw.writeStartElement("RangeOperator");
		xmlw.writeCharacters(study.getRangeOperator());
		xmlw.writeEndElement();
		xmlw.writeCharacters("\n");
		}

		if(study.getValue2() != null){
		xmlw.writeCharacters("        ");
		xmlw.writeStartElement("Value2");
		xmlw.writeCharacters(study.getValue2().toString());
		xmlw.writeEndElement();
		xmlw.writeCharacters("\n");
		}

		if(study.getUnits() != null && !study.getUnits().isEmpty()){
		xmlw.writeCharacters("        ");
		xmlw.writeStartElement("Units");
		xmlw.writeCharacters(study.getUnits());
		xmlw.writeEndElement();
		xmlw.writeCharacters("\n");
		}

		if(study.getValue() != null && !study.getValue().isEmpty()){
		xmlw.writeCharacters("        ");
		xmlw.writeStartElement("Value");
		xmlw.writeCharacters(study.getValue());
		xmlw.writeEndElement();
		xmlw.writeCharacters("\n");
		}

		if(study.getSource().getValue() != null){
		xmlw.writeCharacters("        ");
		xmlw.writeStartElement("Source");
		if(study.getSource().getType() != null || !study.getSource().getType().isEmpty()){
		xmlw.writeAttribute("Type", study.getSource().getType());
		}
		xmlw.writeCharacters(study.getSource().getValue().toString());
		xmlw.writeEndElement();
		xmlw.writeCharacters("\n");
		}

		if (!study.getExperimentalActivity().isEmpty()) {

			for (ExperimentalActivity activity : study
					.getExperimentalActivity()) {
				xmlw.writeCharacters("        ");
				xmlw.writeStartElement("Experimental_Activity");
				xmlw.writeCharacters("\n");

				if(!activity.getSystem().isEmpty() && activity.getSystem() != null){
				xmlw.writeCharacters("            ");
				xmlw.writeStartElement("System");
				xmlw.writeCharacters(activity.getSystem());
				xmlw.writeEndElement();
				xmlw.writeCharacters("\n");
				}

				if(!activity.getEffect().isEmpty() && activity.getEffect() != null){
				xmlw.writeCharacters("            ");
				xmlw.writeStartElement("Effect");
				xmlw.writeCharacters(activity.getEffect());
				xmlw.writeCharacters("\n");
				}

				if(!activity.getTargetConditionToxicity().getContent().isEmpty() && activity.getTargetConditionToxicity().getContent() != null){
				xmlw.writeCharacters("            ");
				xmlw.writeStartElement("Target_condition_toxicity");
				if(!activity.getTargetConditionToxicity().getType().isEmpty() && activity.getTargetConditionToxicity().getType() != null){
				xmlw.writeAttribute("Type", activity
						.getTargetConditionToxicity().getType());
				}
				xmlw.writeCharacters(activity.getTargetConditionToxicity()
						.getContent());
				xmlw.writeEndElement();
				xmlw.writeCharacters("\n");
				}

				xmlw.writeCharacters("        ");
				xmlw.writeEndElement();
				xmlw.writeCharacters("\n");
			}
		}

		xmlw.writeCharacters("    ");
		xmlw.writeEndElement();
		xmlw.writeCharacters("\n");

	}

	private static Connection getDBConnection() throws SQLException,
			IOException {
		Properties prop = new Properties();

		prop.load(Execution.class.getResourceAsStream("config.properties"));

		DriverManager.registerDriver(new oracle.jdbc.OracleDriver());

		Connection conn = null;
		String jdbcUrl = "jdbc:oracle:thin:@" + prop.getProperty("host") + ":"
				+ prop.getProperty("port") + ":" + prop.getProperty("sid");

		String user = prop.getProperty("user");
		String password = prop.getProperty("password");

		conn = DriverManager.getConnection(jdbcUrl, user, password);

		conn.setAutoCommit(false);

		return conn;
	}

}
